## Particle Swarm Optimization Configures the Route Minimization Algorithm

**Tomasz Jastrzab, Michal Myller, Lukasz Tulczyjew, Miroslaw Blocho, Wojciech Ryczko, Michal Kawulok, Jakub Nalepa**

This repository contains a real-life PDPTW dataset from the Gliwice area, used in the manuscript submitted to the International Conference on Computational Science ICCS 2022. 

#### Dataset structure

This dataset consists of 40 real-life PDPTW problem instances. Each problem instance consists of problem definition and a baseline solution in the file named after the problem definition but with the *_sol* suffix.
 
The structure of each problem instance definition follows the SINTEF format described [here](https://www.sintef.no/projectweb/top/pdptw/documentation/):
1. The first line gives the fleet size and capacity of each vehicle. The third value is unused.
2. The consecutive lines describe successive requests consisting of request id, longitude, latitude, demand, left time window margin, right time window margin, service time, pickup id, delivery id.
3. Request with id 0 defines the depot.

The structure of the solutions also follows the SINTEF format and contains a number of lines, where each line corresponds to a single route represented as a set of request ids.  
**Note**: The depot request id is omitted in each route. However, each route starts and ends at the depot.

The naming convention of the files is as follows:  
r&lt;number_of_requests&gt;_vr&lt;average_length_of_requests_per_route&gt;_c&lt;capacity&gt;_w&lt;time_window_span&gt;  

e.g., r100_vr10_c200_w005 is a problem with 100 requests, with an average of 10 requests per route, where each vehicle has a capacity of 200 and the time window span is 0.05 of the actual margin.
